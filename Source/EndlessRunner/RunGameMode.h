// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
private:
	class ATile* lastTileSpawned;

protected:
	virtual void BeginPlay() override;

	void SpawnTile();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Game Mode Variables")
		TSubclassOf<ATile> tileToSpawn;

	UFUNCTION()
		void OnTileExited(class ATile* tile);

};
