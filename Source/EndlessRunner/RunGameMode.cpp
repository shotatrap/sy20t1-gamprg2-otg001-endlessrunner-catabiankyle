// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Tile.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"

void ARunGameMode::SpawnTile()
{
	if (lastTileSpawned == nullptr)
	{
		FVector location(0.0f, 0.0f, 0.0f);
		FRotator rotation(0.0f, 0.0f, 0.0f);
		lastTileSpawned = GetWorld()->SpawnActor<ATile>(tileToSpawn, location, rotation);
		lastTileSpawned->OnTileExited.AddDynamic(this, &ARunGameMode::OnTileExited);
	}
	else
	{
		FVector location = lastTileSpawned->GetAttachPoint()->GetComponentLocation();
		FRotator rotation = lastTileSpawned->GetAttachPoint()->GetComponentRotation();
		lastTileSpawned = GetWorld()->SpawnActor<ATile>(tileToSpawn, location, rotation);
		lastTileSpawned->OnTileExited.AddDynamic(this, &ARunGameMode::OnTileExited);
	}
}

void ARunGameMode::OnTileExited(ATile* tile)
{
	SpawnTile();
}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	for (int32 i = 0; i < 5; i++)
	{
		SpawnTile();
	}
}
