// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"
#include "RunCharacter.h"
#include "Tile.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APickup::APickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("Static Mesh");

}

void APickup::BindListener(ATile* ParentTile)
{
	ParentTile->OnTileDestroy.AddDynamic(this, &APickup::OnTileDestroy);
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnOverlap);
	
}

void APickup::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ACharacter* player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (player == Cast<ACharacter>(OtherActor))
	{
		OnGet(Cast<ARunCharacter>(player));
	}
}

void APickup::OnTileDestroy()
{
	Destroy();
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

