// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExited, class ATile*, Tile);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTileDestroyed);

class AObstacle;
class APickup;

UCLASS()
class ENDLESSRUNNER_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	UPROPERTY(BlueprintAssignable)
	FExited OnTileExited;

	UPROPERTY(BlueprintAssignable)
	FTileDestroyed OnTileDestroy;

	FTimerHandle TileDestroyHandle;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Variables")
	TArray<TSubclassOf<AObstacle>> ObstacleArray;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Variables")
		TArray<TSubclassOf<APickup>> PickupArray;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UArrowComponent* AttachPoint;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* ExitTrigger;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* SpawnArea;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* PickupSpawnArea;

	UFUNCTION()
		void OnHit(UPrimitiveComponent* Collider, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void DestroyTile();

	void SpawnObstacle();

	void SpawnPickup();

	void ChanceSpawner();

public:
	virtual void Tick(float DeltaTime) override;

	UArrowComponent* GetAttachPoint();
};
