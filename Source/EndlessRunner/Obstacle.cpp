// Fill out your copyright notice in the Description page of Project Settings.


#include "Obstacle.h"
#include "Components/StaticMeshComponent.h"
#include "RunCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Tile.h"

// Sets default values
AObstacle::AObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("Static Mesh");

}

void AObstacle::BindListener(ATile* ParentTile)
{
	ParentTile->OnTileDestroy.AddDynamic(this, &AObstacle::OnTileDestroy);
}

// Called when the game starts or when spawned
void AObstacle::BeginPlay()
{
	Super::BeginPlay();
	StaticMesh->OnComponentHit.AddDynamic(this, &AObstacle::OnHit);
}

void AObstacle::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ACharacter* player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (player == Cast<ACharacter>(OtherActor))
	{
		OnTrigger(Cast<ARunCharacter>(player));
	}
}

void AObstacle::OnTileDestroy()
{
	Destroy();
}

// Called every frame
void AObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

