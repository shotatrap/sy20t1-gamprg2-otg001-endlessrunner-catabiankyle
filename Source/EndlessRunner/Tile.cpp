// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "RunCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Obstacle.h"
#include "Kismet/KismetMathLibrary.h"
#include "Pickup.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("Scene Component");
	SetRootComponent(SceneComponent);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>("Attach Point");
	AttachPoint->SetupAttachment(SceneComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>("Exit Trigger");
	ExitTrigger->SetupAttachment(SceneComponent);

	SpawnArea = CreateDefaultSubobject<UBoxComponent>("Spawn Trigger");
	SpawnArea->SetupAttachment(SceneComponent);

	PickupSpawnArea = CreateDefaultSubobject<UBoxComponent>("Pickup Spawn Trigger");
	PickupSpawnArea->SetupAttachment(SceneComponent);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	ExitTrigger->OnComponentEndOverlap.AddDynamic(this, &ATile::OnHit);
	ChanceSpawner();
}

void ATile::OnHit(UPrimitiveComponent* Collider, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ACharacter* player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (player == Cast<ACharacter>(OtherActor))
	{
		OnTileExited.Broadcast(this);
		GetWorldTimerManager().SetTimer(TileDestroyHandle, FTimerDelegate::CreateUObject(this, &ATile::DestroyTile), 3.0f, false);
	}
}

void ATile::DestroyTile()
{
	OnTileDestroy.Broadcast();
	Destroy();
}

void ATile::SpawnObstacle()
{
	FVector spawnPos = UKismetMathLibrary::RandomPointInBoundingBox(SpawnArea->GetComponentLocation(), SpawnArea->GetScaledBoxExtent());
	FRotator rotation(0.0f, 0.0f, 0.0f);

	AObstacle* spawnedObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleArray[FMath::RandRange(0, ObstacleArray.Num() - 1)], spawnPos, rotation);
	spawnedObstacle->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	spawnedObstacle->BindListener(this);
}

void ATile::SpawnPickup()
{
	FVector spawnPos = UKismetMathLibrary::RandomPointInBoundingBox(PickupSpawnArea->GetComponentLocation(), PickupSpawnArea->GetScaledBoxExtent());
	FRotator rotation(0.0f, 0.0f, 0.0f);

	APickup* spawnedPickup = GetWorld()->SpawnActor<APickup>(PickupArray[FMath::RandRange(0, PickupArray.Num() - 1)], spawnPos, rotation);
	spawnedPickup->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	spawnedPickup->BindListener(this);
}

void ATile::ChanceSpawner()
{
	bool spawnObs = UKismetMathLibrary::RandomBoolWithWeight(.60);
	bool spawnPickup = UKismetMathLibrary::RandomBoolWithWeight(.30);

	int32 numOfPickup = FMath::RandRange(1, 3);

	if (spawnObs)
	{
		SpawnObstacle();
	}
	if (spawnPickup)
	{
		for (int32 i = 0; i < numOfPickup; i++)
		{
			SpawnPickup();
		}
	}
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

UArrowComponent* ATile::GetAttachPoint()
{
	return AttachPoint;
}

